public class Movie {
    private final String title;
    private final String studio;
    private final String rating;

    public Movie(String title, String studio, String rating) {
        this.title = title;
        this.studio = studio;
        this.rating = rating;
    }

    public Movie(String title, String studio) {
        this.title = title;
        this.studio = studio;
        this.rating = "PG";
    }

    public static Movie[] getPg(Movie[] movie) {
        Movie[] pgMovie = new Movie[movie.length];

        int arrayIndex = 0;
        for (int i = 0; i < movie.length; i++) {
            if (movie[i].rating == "PG") {
                pgMovie[arrayIndex] = movie[i];
                arrayIndex++;
            }
        }
        return pgMovie;
    }

    public static void main(String[] args) {
        Movie movie = new Movie("Casino Royale", "Eon Productions", "PG-13");
        System.out.println(movie.rating);
    }
}

